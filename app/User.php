<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address_id',
        'birthday',
        'specialize_id',
        'gender',
        'password',
        'is_admin',
        'stt'
    ];

    public function address()
    {
        return $this->belongsTo('App\Address', 'address_id');
    }

    public function specialize()
    {
        return $this->belongsTo('App\Specialize', 'specialize_id');
    }

    public function getAll()
    {
        return User::where('status', '0')->where('is_admin', '0')->get();
    }

    public function getOneStudent($id)
    {
        return User::findOrFail($id);
    }

    public function countStudentOfSpecialize($id)
    {
        return User::where('specialize_id', $id)->where('is_admin', 0)->count();
    }

    public function findStudent($name, $email, $phone, $address_id, $specialize_id, $gender)
    {
        $users =  User::with(['Address', 'Specialize'])->where('name', 'like', '%' . $name . '%')
            ->where('email', 'like', '%' . $email . '%')
            ->where('phone', 'like', '%' . $phone . '%')

            ->where('gender', 'like', '%' . $gender . '%');
        if ($address_id){
            $users = $users->where('address_id',$address_id) ;
        }
        if ($specialize_id){
            $users = $users->where('specialize_id', $specialize_id) ;
        }
        return $users->get() ;
    }
}
