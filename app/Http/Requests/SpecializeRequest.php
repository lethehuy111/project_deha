<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class SpecializeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = ($request->id)?$request->id : "";
        return [
            'name_specialize' => 'required|unique:specializes,name_specialize,'.$id
        ];
    }
    public function messages()
    {
        return [
            'name_specialize.required'=> 'Không bỏ trống tên chuyên ngành' ,
            'name_specialize.unique' => 'Tên chuyên ngành đã tồn tại'
        ];
    }
}
