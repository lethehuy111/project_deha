<?php

namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session('is_admin') == 0) {
            return redirect('user');
        } elseif (Session('is_admin') == 1) {
            return $next($request);
        } else {

            return redirect('login');
        }

    }
}
