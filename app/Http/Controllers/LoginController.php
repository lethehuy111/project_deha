<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Session;
use App\User ;
class LoginController extends Controller
{
    public function index(){
        return view('login');
    }
    public function welcome(){
        return view('welcome');
    }
    public function postLogin(LoginRequest $request){
        $email = $request->input('email');
        $password = $request->input('password');
        $user = User::where('email', $email)->where('password',md5($password))->first();
        if ($user){
            session()->put('is_admin', $user->is_admin );
            return redirect('/');
        }
        else{
            $info = "Tài khoản hoặc mật khẩu chưa đúng" ;
            return view('login', compact('info','email','password'));
        }
    }

}
