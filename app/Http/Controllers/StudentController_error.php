<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\User;
use App\Address;
use App\Specialize;

class StudentControllerError extends Controller
{
    protected $address;
    protected $subject;
    protected $specialize;
    protected $user;

    function __construct()
    {
        $this->address = new Address();
        $this->subject = new Subject();
        $this->specialize = new Specialize();
        $this->user = new User();
    }

    public function index()
    {
        $specializes = $this->specialize->getAll();
        $address = $this->address->getAll();
        $students = $this->user->getAll();

        return view('top-page', compact('students', 'address', 'specializes'));
    }

    public function addStudent()
    {
        $address = $this->address->getAll();
        $specializes = $this->specialize->getAll();
        return view('student/add', compact('address', 'specializes'));
    }

    public function editStudent($id)
    {
        $address = $this->address->getAll();
        $student = $this->user->getOneStudent($id);
        $specializes = $this->specialize->getAll();
        return view('student/edit', compact('address', 'specializes', 'student'));
    }
}
