<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialize;
use App\User;
use App\Http\Requests\SpecializeRequest;
class SpecializeController extends Controller
{
    protected $user;
    protected $specialize;

    function __construct()
    {
        $this->user = new User();
        $this->specialize = new Specialize();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specializes = $this->specialize->getAll();
        return view('specialize/index', compact('specializes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpecializeRequest $request)
    {
//        $specialize = $this->specialize->add($request->all());
        $specialize = Specialize::create($request->all());
        return response()->json(['message' => $specialize]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $specialize =  $this->specialize->getSpecialize($id);
        return response()->json([
            'data'=> $specialize
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SpecializeRequest $request, $id)
    {
        $specialize = $this->specialize->getSpecialize($id);
        $specialize->update($request->all());
        return response()->json([
            "data" => $specialize
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $numberStudent = $this->user->countStudentOfSpecialize($id);
        if ($numberStudent > 0) {
            return false;
        }else{
            $delete = $this->specialize->deleteRecord($id);
            return response()->json(["messager" => "Xóa Thành Công"]);
        }

    }
}
