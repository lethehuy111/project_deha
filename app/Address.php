<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';

    public function getAll(){
        return Address::all()  ;
    }
    public function getAddress($id){
        return Address::find($id);
    }
}
