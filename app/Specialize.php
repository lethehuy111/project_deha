<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialize extends Model
{
    protected $table = 'specializes';
    protected $fillable = [
        'name_specialize'
    ];
    public $timestamps = false;

    public function user()
    {
        return $this->hasMany('App\User', 'specialize_id', 'id');
    }

    public function getAll()
    {
        return Specialize::withCount(['user'])->get();
    }

    public function getSpecialize($id)
    {
        return Specialize::find($id);
    }

    public function deleteRecord($id)
    {
        return Specialize::destroy($id);
    }

    public function add($request)
    {
        return Specialize::create($request);
    }
    //
}
