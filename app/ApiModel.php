<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiModel extends Model
{
    protected $table = "users";
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address_id',
        'birthday',
        'specialize_id',
        'gender',
        'password',
        'is_admin',
        'status',
    ];
}
