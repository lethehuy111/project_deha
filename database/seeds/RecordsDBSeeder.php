<?php

use Illuminate\Database\Seeder;

class RecordsDBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('address')->insert([
                ['name_city' => 'Bắc Ninh'], ['name_city' => 'Hà Nam'], ['name_city' => 'Hà Nội'],
                ['name_city' => 'Hải Dương'], ['name_city' => ' Hưng Yên'], ['name_city' => ' Hải Phòng'],
                ['name_city' => 'Ninh Bình'], ['name_city' => 'Thái Bình'], ['name_city' => 'Thanh Hóa']]
        );
        DB::table('specializes')->insert([
            ['name_specialize' => 'Công nghệ thông tin'], ['name_specialize' => 'Kỹ thuật điện-điện tử'],
            ['name_specialize' => 'An toàn thông tin'], ['name_spceialize' => 'Điện Tử viễn thông']
        ]);

        DB::table('users')->insert([
            ['name' => 'Lê Thế Huy',
                'email' => 'lethehuya6@gmail.com',
                'phone' => '0904534320',
                'address_id' => 1,
                'birthday' => '1997-12-19',
                'specialize_id' => 1,
                'is_admin' => 1,
                'gender' => 1,
                'status' => 1
            ]
        ]);
        DB::table('subjects')->insert([
            ['specialize_id'=> 1 , 'name_subject'=> 'PHP'],['specialize_id'=> 1 , 'name_subject'=> 'Javascript'],['specialize_id'=> 1 , 'name_subject'=> 'Python'],
        ['specialize_id'=> 2 , 'name_subject'=> 'Lập trình nhúng'],['specialize_id'=> 2 , 'name_subject'=> 'IOT Arduino'],['specialize_id'=> 2 , 'name_subject'=> '8051'],
        ['specialize_id'=> 3 , 'name_subject'=> 'Điều tra mạng'],['specialize_id'=> 3 , 'name_subject'=> 'An ninh cơ sở dữ liệu'],
        ['specialize_id'=> 4 , 'name_subject'=> 'Cơ sở kỹ thuật thông tin vô tuyến'], ['specialize_id'=> 4 , 'name_subject'=> 'Cơ sở kỹ thuật mạng truyền thông']
    ]);
        //
    }
}
