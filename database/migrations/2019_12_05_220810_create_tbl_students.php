<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('password')->default(md5('1234'));
            $table->string('phone');
            $table->bigInteger('address_id')->unsigned() ;
            $table->date('birthday');
            $table->bigInteger('specialize_id')->unsigned();
            $table->tinyInteger('gender');
            $table->tinyInteger('is_admin')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
