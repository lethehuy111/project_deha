$(function () {
    function countStt() {
        var renum = 1;
        $("tr td strong").each(function () {
            $(this).text(renum);
            renum++;
        });
    }

    countStt();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function callAjax(url, method, data) {
        return $.ajax({
            url: url,
            type: method,
            data: data
        });
    }

    $(document).on("click", ".delete", function () {
        // Bật sweetalert
        var id = $(this).val();
        swal({
            title: "Bạn chắc chắn muốn xóa sinh viên này?",
            text: "Khi xóa sẽ không khôi phục lại được nữa!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    //Nếu xác nhận xóa thực hiện gọi Ajax xóa sinh viên
                    callAjax("/api/users/" + id, "DELETE", id)
                        .done(function (data) {
                            swal("Xóa Thành Công!", "", "success");
                            $('.tr' + id).remove();
                            // $(".pagination").load(".pagination");
                            countStt();
                        })
                        .fail(function (error) {
                            swal("Xóa Không Th Công!", "", "warning");
                        })
                    //Kết thúc Xóa sinh v
                }
            });

    });
    $('.reset').on("click", function (e) {
        e.preventDefault();
        $(".form-search")[0].reset();
    });
    $('.sm-search').on("click", function (e) {
        e.preventDefault();
        let data = $(".form-search").serialize();
        console.log(data);
        callAjax("/student", "POST", data)
            .done(function (response) {
                console.log(response);
                let resultSearch = response.data;
                $('.results-search tbody').html('');
                for (var i = 0; i < resultSearch.length; i++) {
                    let tableHtml = ` <tr class="tr${resultSearch[i].id}">
                        <td><strong></strong></td>
                        <td>${resultSearch[i].name}</td>
                        <td>${resultSearch[i].email} </td>
                        <td>${resultSearch[i].phone}</td>
                        <td>${(resultSearch[i].gender == 1) ? "Nam" : "Nữ"}</td>
                        <td>${resultSearch[i].address.name_city}</td>
                        <td>${resultSearch[i].specialize.name_specialize}</td>
                        <td>
                            <button value="${resultSearch[i].id}" class="btn btn-danger action delete">Xóa</button>
                            <a class="btn btn-success action" href="http://localhost/student/${resultSearch[i].id}/edit"> Sửa</a>
                        </td>`;
                    $('.results-search tbody').append(tableHtml);
                }
                countStt();
                getPagination('#table-id');
            })
            .fail(function (error) {
                console.log(error);
            })
    });

    getPagination('#table-id');

    function getPagination(table) {
        var lastPage = 1;

        $('#maxRows')
            .on('change', function (evt) {
                //$('.paginationprev').html('');						// reset pagination

                lastPage = 1;
                $('.pagination')
                    .find('li')
                    .slice(1, -1)
                    .remove();
                var trnum = 0; // reset tr counter
                var maxRows = parseInt($(this).val()); // get Max Rows from select option

                if (maxRows == 5000) {
                    $('.pagination').hide();
                } else {
                    $('.pagination').show();
                }

                var totalRows = $(table + ' tbody tr').length; // numbers of rows
                $(table + ' tr:gt(0)').each(function () {

                    trnum++; // Start Counter
                    if (trnum > maxRows) {
                        // if tr number gt maxRows

                        $(this).hide(); // fade it out
                    }
                    if (trnum <= maxRows) {
                        $(this).show();
                    } // else fade in Important in case if it ..
                }); //  was fade out to fade it in
                if (totalRows > maxRows) {
                    // if tr total rows gt max rows option
                    var pagenum = Math.ceil(totalRows / maxRows); // ceil total(rows/maxrows) to get ..
                    //	numbers of pages
                    for (var i = 1; i <= pagenum;) {
                        // for each page append pagination li
                        $('.pagination #prev')
                            .before(
                                '<li data-page="' +
                                i +
                                '">\
                                                  <span>' +
                                i++ +
                                '<span class="sr-only">(current)</span></span>\
                                                </li>'
                            )
                            .show();
                    } // end for i
                } // end if row count > max rows
                $('.pagination [data-page="1"]').addClass('active'); // add active class to the first li
                $('.pagination li').on('click', function (evt) {
                    // on click each page
                    evt.stopImmediatePropagation();
                    evt.preventDefault();
                    var pageNum = $(this).attr('data-page'); // get it's number

                    var maxRows = parseInt($('#maxRows').val()); // get Max Rows from select option

                    if (pageNum == 'prev') {
                        if (lastPage == 1) {
                            return;
                        }
                        pageNum = --lastPage;
                    }
                    if (pageNum == 'next') {
                        if (lastPage == $('.pagination li').length - 2) {
                            return;
                        }
                        pageNum = ++lastPage;
                    }

                    lastPage = pageNum;
                    var trIndex = 0; // reset tr counter
                    $('.pagination li').removeClass('active'); // remove active class from all li
                    $('.pagination [data-page="' + lastPage + '"]').addClass('active'); // add active class to the clicked
                    // $(this).addClass('active');					// add active class to the clicked
                    limitPagging();
                    $(table + ' tr:gt(0)').each(function () {
                        // each tr in table not the header
                        trIndex++; // tr index counter
                        // if tr index gt maxRows*pageNum or lt maxRows*pageNum-maxRows fade if out
                        if (
                            trIndex > maxRows * pageNum ||
                            trIndex <= maxRows * pageNum - maxRows
                        ) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        } //else fade in
                    }); // end of for each tr in table
                }); // end of on click pagination list
                limitPagging();
            })
            .val(5)
            .change();
    }

    function limitPagging() {
        if ($('.pagination li').length > 7) {
            if ($('.pagination li.active').attr('data-page') <= 3) {
                $('.pagination li:gt(5)').hide();
                $('.pagination li:lt(5)').show();
                $('.pagination [data-page="next"]').show();
            }
            if ($('.pagination li.active').attr('data-page') > 3) {
                $('.pagination li:gt(0)').hide();
                $('.pagination [data-page="next"]').show();
                for (let i = (parseInt($('.pagination li.active').attr('data-page')) - 2); i <= (parseInt($('.pagination li.active').attr('data-page')) + 2); i++) {
                    $('.pagination [data-page="' + i + '"]').show();
                }

            }
        }
    }


});
