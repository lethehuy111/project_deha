$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function newResource(data, url) {
        return $.ajax({
            url: url,
            type: "PATCH",
            data: data
        });
    };

    function ShowError(array_error) {
        var arr_field = [array_error.name, array_error.email, array_error.phone, array_error.gender,
            array_error.id_address, array_error.birthday, array_error.id_specialize];
        var list_error = "";
        $('.show_error').html(list_error);
        for (var i = 0; i < arr_field.length; i++) {
            if (arr_field[i]) {
                $('.show_error').append('<li>' + arr_field[i] + '</li>');
            }
        }
    }

    $("form :input").change(function () {
        $('.sm_edit').removeAttr('disabled');
        $('.sm_edit').on("click", function (e) {
            e.preventDefault();
            var id = $(this).val();
            data = $('.edit-resource').serialize();
            console.log(data);
            var url = "/api/users/"+id;
            newResource(data, url)
                .done(response => {
                    console.log(response);
                    if (response.status_code == '400') {
                        ShowError(response.message_error);
                    }
                    if (response.status_code == '200') {
                        // Hien sweetaleat ///
                        $('.show_error').html("");
                        swal({
                            title: "Cập nhật thành công",
                            text: "",
                            icon: "success",
                            dangerMode: true,
                            buttons: {
                                cancel: "Ở lại trang",
                                redirect: {
                                    text: "Chuyển về trang chủ",
                                    value: "redirect",
                                },
                            },
                        })
                            .then((value) => {
                                if (value == 'redirect') {
                                    window.location.href = "http://localhost/";
                                } else {
                                    $('.add-resource')[0].reset();
                                }
                            });
                    }
                })
                .fail(error => {
                    console.log(error);
                });
        });
    });
});
