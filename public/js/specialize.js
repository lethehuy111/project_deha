$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function callAjax(url, type, data) {
        return $.ajax({
            url: url,
            type: type,
            data: data
        })
    }

    function countStt() {
        var renum = 1;
        $("tr td strong").each(function () {
            $(this).text(renum);
            renum++;
        });
    }

    countStt();
    // filter table

    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    //add specialize
    $('.add-specialize').on("click" , function (e) {
        e.preventDefault();
        $('.error-name').html("");
        var data = $('.form-add').serialize();
        callAjax("/specialize" , "POST",data)
            .done(function (data) {
               // console.log(data);
                $('#addSpecialize').modal('hide');
                swal("Thêm Mới Thành Công!", "", "success");

                var htmlTable = `<tr class="tr${data.message.id}">
                    <td><strong></strong></td>
                    <td>${data.message.name_specialize}</td>
                    <td >0</td>
                    <td>
                        <button value="${data.message.id}" class="btn btn-success action edit">Sửa</button>
                        <button value="${data.message.id}" class="btn btn-danger action delete">Xóa</button>
                    </td>
                </tr>` ;
                $('.table-specialize').append(htmlTable) ;
                countStt();
            })
            .fail(function (error) {
                //console.log(error);
                $('.error-name').html(error.responseJSON.errors.name_specialize);
            })
    });
    //delele specialize
    $(document).on("click", ".delete" ,function () {
        var id = $(this).val() ;
        swal({
            title: "Bạn chắc chắn muốn xóa khoa này không?",
            text: "Khi xóa sẽ không khôi phục lại được nữa!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    //Nếu xác nhận xóa thực hiện gọi Ajax xóa sinh viên
                    callAjax("/specialize/"+ id , "DELETE", id)
                        .done(function (data) {
                            swal("Xóa Thành Công!", "", "success");
                            $('.tr'+id).remove();
                            countStt();
                        })
                        .fail(function () {
                            swal("Xóa Không Thành Công!", "Vẫn Còn học sinh trong khoa!", "warning");
                        });
                }
            });

    });
    //edit specialize
    $(document).on("click" , ".edit" ,function () {
        let id = $(this).val();
        callAjax('/specialize/'+ id ,'GET',id )
            .done(function (data) {
               // console.log(data);
                $(".input-specialize").val(data.data.name_specialize);
                $(".id-specialize").val(data.data.id);
                $(".error-edit-name").html(" ");
                $("#editSpecialize").modal('show');
                $('.edit-specialize').on("click" , function (e) {
                    e.preventDefault();
                    let data =  $('.form-edit').serialize();

                    callAjax('/specialize/'+id,'PATCH' ,data )
                        .done(function (response) {
                            console.log(response);
                            $("#editSpecialize").modal('hide');
                            swal("Thay Đổi Thành Công!", "", "success");
                            $(".td"+id).html(response.data.name_specialize);
                        })
                        .fail(function (error) {
                            console.log(error);
                            $('.error-edit-name').html(error.responseJSON.errors.name_specialize);
                        })
                })
            })
    })
});
