<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/my_css.css')}}">
    <link rel="stylesheet" href="{{ asset('css/css-admin.css') }}">
    <link rel="stylesheet" href="{{asset('css/css-confirm.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
</head>
<body>
<div class="container">
    <div class="row">
        <nav class="navbar navbar-expand-sm bg-light navbar-light header">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{asset('/')}}" style="width: 145px ;"><h4>Logo</h4></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('/')}}">Danh sách sinh viên</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('specialize')}}">Quản lý ngành</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ asset('student/create') }}">Thêm sinh viên</a>
                </li>
                <div class="dropdown" style="margin-left: 8%  ;">
                    <button type="button" class="btn btn-primary dropdown-toggle acction" data-toggle="dropdown">
                        Lê Thế Huy
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Thông tin cá nhân</a>
                        <a class="dropdown-item" href="{{asset('logout')}}">Đăng xuất</a>
                    </div>
                </div>
            </ul>
        </nav>
    </div>

    <!-- ---------------------Banner Trang web------------------------------------------ -->
    <div class="row">
        <img src="{{ asset('images/banner-it.jpg') }}" width="100%">
    </div>
    <!-- -------------------------Kết thúc banner trang web---------------------------------- -->
    <div class="row main">
        <!-- -----------Menu Quản lý------------------------------------------------------ -->
        <div class=" main-right">
            @yield('content')
        </div>
    </div>
    <!-- --------------------------footer------------------------------ -->
    <div class="footer">
        <p> Copyright ©20199 Lê Thế Huy </p>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @yield('LinkJs')
</div>
</body>
</html>
