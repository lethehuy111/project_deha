@extends('layout.app')

@section('title', 'Sửa thông tin')
@section('content')

    <!-- --------------Nhập form thêm người dùng------------------------------------ -->
    <div class="add-student">
        <form action="" method="POST" enctype="multipart/form-data" class = "edit-resource">
            <legend style="margin-left: 0px; margin-bottom: 20px ; ">Sửa thông tin sinh viên</legend>
            <div class="error" style="width: 100%;">
                <ul class="show_error">
                    {{--                <li>Hiển thị lỗi ở đây</li>--}}
                </ul>
            </div>
            <div class="form-group">
                <label for="">Tên sinh viên :<span style="color: red ;">(*)</span></label>
                <input type="text" required class="form-control" name="name" value="{{$student->name}}">
            </div>
            <div class="form-group">
                <label for="">Email :<span style="color: red ;">(*)</span></label>
                <input type="email" required class="form-control" name="email" value="{{$student->email}}">
            </div>

            <div class="">
                <label for="">Số điện thoại :<span style="color: red ;">(*)</span></label>
                <input type="number" required class="form-control" name="phone" value="{{$student->phone}}">
            </div>
            {{--                            Ngay sinh--}}
            <div class="form-group">
                <label for="example-datetime" ">Ngày sinh<span style="color: red ;">(*)</span></label>
                <input class="form-control" required type="date" value="{{$student->birthday}}" name="birthday" size="10">

            </div>

            <!-- ---------------------------------giới tính------------------------------------ -->
            <label for="">Giới tính :<span style="color: red ;">(*)</span></label>
            <div class="form-group">
                <input type="radio" class="" id="nam" value="1" name="gender" {{($student->gender == 1)?'checked':''}}>
                <label class="" for="nam">Nam</label>
                <input type="radio" class="" id="nu" value="0" name="gender" {{($student->gender == 0)?'checked':''}}>
                <label class="" for="nu">Nữ</label>
            </div>
            <!-- ---------------------------checkbox ngôn ngữ lập trình-------------------------------- -->

            <label for="">Khoa đào tạo :<span style="color: red ;">(*)</span></label>
            <select class="form-control" required name="specialize_id">
                @foreach($specializes as $specialize)
                    <option value="{{$specialize->id}}"{{($specialize->id == $student->specialize_id)?'selected':'' }}>{{ $specialize->name_specialize }}</option>
                @endforeach
            </select>
            <!-- ----------------------select address--------------------- -->
            <br>
            <label for="">Địa Chỉ :<span style="color: red ;">(*)</span></label>
            <select class="form-control" required name="address_id">
                @foreach($address as $item)
                    <option value="{{$item->id}}" {{($item->id == $student->address_id )?'selected':''}}>{{ $item->name_city }}</option>
                @endforeach
            </select>
            <br>
            <button type="submit" name="add_member" class="btn btn-primary sm_edit" style="margin-left: 0px;" value="{{$student->id}}" disabled>Thay đổi</button>
        </form>
    </div>
@endsection
@section('LinkJs')
    <script src="{{ asset('js/edit-student.js') }}"></script>
@endsection
