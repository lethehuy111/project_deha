@extends('layout.app')

@section('title', 'Trang Chủ')

@section('content')
    <div class="search-user">
        <p><b>Tìm kiếm người dùng</b></p>
    </div>
    <div class="content-search">

        <!-- -------------------Bảng tìm kiếm thông tin ---------------------- -->
        <form action="{{asset('student')}}" method="" class="form_search form-search">
            <table id="dtBasicExample" class="table-search">
                <tr>
                    <td>Email</td>
                    <td><input type="text" class="form-control" name="email" value=""
                               size="50%" style=" height: 30px;border-radius: 2px"></td>
                    <td>Giới tính</td>
                    <td>
                        <input type="radio" name="gender" value="1">Nam
                        <input type="radio" name="gender" value="0">Nữ
                        <input type="radio" name="gender" value="" checked>Cả hai
                    </td>
                </tr>
                <tr>
                    <td>Tên Thật</td>
                    <td><input type="text" class="form-control" name='name' value="" size="40%"
                               style="	height: 30px;border-radius: 2px"></td>

                    <td>Khoa</td>
                    <td>
                        <select class="form-control" name="specialize_id">
                            <option value="">---Chọn Khoa---</option>
                            @foreach($specializes as $specialize)
                                <option value="{{$specialize->id}}">{{$specialize->name_specialize}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Số điện thoại</td>
                    <td><input type="number" class="form-control" name='phone' value="" size="40%"
                               style="	height: 30px;border-radius: 2px"></td>

                    <td>Địa chỉ</td>
                    <td>
                        <select class="form-control" name="address_id">
                            <option value="">---Địa chỉ---</option>
                            @foreach($address as $item)
                                <option value="{{$item->id}}">{{ $item->name_city}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <button type="submit" name="sm_search" class="btn btn-primary sm-search">Tìm
                            kiếm
                        </button>
                        <button type="submit" name="" class="btn btn-danger reset">Reset</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <!-----	Hiển thị kết quả tìm kiếm -->
    <div>
        <!-- -------------------Danh sách người dùng-------------------------->
        <div class="search-user">
            <p><b>Danh sách người dùng</b></p>
        </div>
    </div>
    <div class="row text-center results-search">
        <table class="table table-striped table-bordered" id="table-id">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên thật</th>
                <th>Email</th>
                <th>Số điện thoại</th>
                <th>Giới tính</th>
                <th>Địa chỉ</th>
                <th>Khoa</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody>

            @foreach($students as $student)
                <tr class="tr{{$student->id}}">
                    {{--                    <td>1</td>--}}
                    <td><strong></strong></td>
                    <td>{{$student->name}}</td>
                    <td>{{$student->email}} </td>
                    <td>{{$student->phone }}</td>
                    <td>{{($student->gender == 1)?'Nam':'Nữ'}}</td>
                    <td>{{$student->address->name_city}}</td>
                    <td>{{$student->specialize->name_specialize}}</td>
                    <td>
                        <button value="{{$student->id}}" class="btn btn-danger action delete">Xóa</button>
                        <a class="btn btn-success action" href="{{asset('student/'.$student->id.'/edit')}}"> Sửa</a>
                    </td>
        @endforeach
    </div>
    </tbody>
    <!-- ------------------hiển thị nội dung trong bảng user -DB------------ -->
    </table>
    </div>
    <div class="form-group number-record">    <!--		Show Numbers Of Rows 		-->
        <select class="form-control" name="state" id="maxRows">
            <option value="5000">Show ALL</option>
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
            <option value="50">50</option>
            <option value="70">70</option>
            <option value="100">100</option>
        </select>
    </div>
    <!--		Start Pagination -->
    <div class='pagination-container'>
        <nav>
            <ul class="pagination">

                <li data-page="prev">
                    <span> Previous <span class="sr-only">(current)</span></span>
                </li>
                <!--	Here the JS Function Will Add the Rows -->
                <li data-page="next" id="prev">
                    <span> Next <span class="sr-only">(current)</span></span>
                </li>
            </ul>
        </nav>
    </div>
    </div>
    </div>
@endsection
@section('LinkJs')
    <script src="{{asset('js/top-page.js')}}"></script>
@endsection
