@extends('layout.app')
@section('title', 'Quản lý ngành')
@section('content')
    <div class="container mt-3">
        <input class="form-control" id="search" type="text" placeholder="Tìm kiếm..." size="10">
        <button class="btn btn-success " data-toggle="modal" data-target="#addSpecialize">Thêm mới</button>
        <br>
        <table class="table table-hover table-specialize">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên chuyên ngành</th>
                <th class = "test" value = "test111">Số lương sinh viên</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody id="myTable">

            @foreach($specializes as $specialize )
                <tr class="tr{{$specialize->id}}">
                    <td><strong></strong></td>
                    <td class="td{{$specialize->id}}">{{$specialize->name_specialize}}</td>
                    <td >{{$specialize->user_count}}</td>
                    <td>
                        <button value="{{$specialize->id}}" class="btn btn-success action edit">Sửa</button>
                        <button value="{{$specialize->id}}" class="btn btn-danger action delete">Xóa</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- The Modal -->
        <div class="modal fade" id="addSpecialize">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Thêm mới ngành học</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" class="form-add">
                            <div class="form-group">
                                <label for="usr">Tên chuyên ngành :<span class="error-name"></span></label>
                                <input type="text" class="form-control" name="name_specialize">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success add-specialize">Thêm mới</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>

{{--        Modal show edit specialize --}}
        <div class="modal fade" id="editSpecialize">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Sửa ngành học </h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST" class="form-edit">
                            <div class="form-group">
                                <label for="usr">Tên chuyên ngành :<span class="error-edit-name"></span></label>
                                <input type="hidden" class="form-control id-specialize" name="id">
                                <input type="text" class="form-control input-specialize" name="name_specialize">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success edit-specialize">Thay đổi</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>

    </div>
@endsection
@section('LinkJs')
    <script src="{{asset('js/specialize.js')}}"></script>
@endsection
